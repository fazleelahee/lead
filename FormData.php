<?php
class FormData
{
	private $creative = 19670;
	private $form = 353;
	private $sitePage = 147259;
	private $title = '';
	private $email = '';
	private $first_name = '';
	private $last_name = '';
	private $ip = '';
	private $metaDisplayed = '';
	private $tpg_23 = ''; //: MANDATORY string (UK mobile telephone number)
	private $tpg_92 = 'gbr'; //: MANDATORY string (country of residence)

	private $optInEmail = true; //: MANDATORY string (always set to true)
	private $optInTelephone = true;
	private $optInPost = true; // : MANDATORY string (always set to true)

	public function __construct()
	{
		$this->metaDisplayed = strtotime('NOW');
		$this->ip = $_SERVER['REMOTE_ADDR'];

		return $this;
	}

	public function setTpg23($tpg_23)
	{
		if (strlen($tpg_23) <= 10 || strlen($tpg_23) >= 20) {
			throw new Exception('Please enter valid phone number!');
		} else {
			$this->tpg_23 = $tpg_23;
			return $this;
		}
	}

	public function getTpg23()
	{
		return $this->tpg_23;
	}

	public function getIp()
	{
		return $this->ip;
	}

	public function setLastName($last_name)
	{
		if (trim($last_name) != '') {
			$this->last_name = sanitize_text_field($last_name);
			return $this;
		} else {
			throw new Exception('Please enter last name');
		}

	}

	public function getLastName()
	{
		return $this->last_name;
	}



	public function setEmail($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$this->email = sanitize_email($email);
			return $this;
		} else {
			throw new Exception('Please enter valid email');
		}
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setFirstName($first_name)
	{
		if (trim($first_name) != '') {
			$this->first_name = sanitize_text_field($first_name);
			return $this;
		} else {
			throw new Exception('Please enter first name');
		}
	}

	public function getFirstName()
	{
		return $this->first_name;
	}


	public function setTitle($title)
	{
		$this->title = sanitize_text_field($title);
		return $this;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setOptInEmail($optInEmail)
	{
		if($optInEmail == '1'){
			$this->optInEmail = true;
		}
		else {
			throw new Exception('You must select email');
		}

		return $this;
	}

	public function getOptInEmail()
	{
		return $this->optInEmail;
	}

	public function setOptInPost($optInPost)
	{
		if($optInPost != '1'){
			$this->optInPost = false;
		}
		return $this;
	}

	public function getOptInPost()
	{
		return $this->optInPost;
	}

	public function setOptInTelephone($optInTelephone)
	{
		if($optInTelephone == '1'){
			$this->optInTelephone = true;
		} else {
			throw new Exception('You must select phone');
		}
		return $this;
	}

	public function getOptInTelephone()
	{
		return $this->optInTelephone;
	}

	public function hydrate(array $data){
        foreach($data as $key=>$val){
            if(property_exists($this, $key)){
                $this->$key = sanitize_text_field($val);
            }
        }
	}

    public function configureFormData($config){
        $atts = @unserialize(base64_decode($config));
        if(is_array($atts) && count($atts) > 0){
            $this->hydrate($atts);
        }

        return $this;
    }

	public function toArray()
	{
		return get_object_vars($this);
	}
}
