/**
 * Created with JetBrains PhpStorm.
 * User: Fazle
 * Date: 15/08/13
 * Time: 03:21
 * To change this template use File | Settings | File Templates.
 */
//namespace
window.App = {};
(function ($) {
    "use strict";
    App.LeadEngine = {
        init: function () {
            App.LeadForm.reset();
            var data = {};
            data.config = App.LeadForm.config();
            data.title = App.LeadForm.title();
            data.first_name = App.LeadForm.first_name();
            data.last_name = App.LeadForm.last_name();
            data.phone = App.LeadForm.phone();
            data.email = App.LeadForm.email();
            data.byEmail = App.LeadForm.byEmail();
            data.byPhone = App.LeadForm.byPhone();
            data.byPost = App.LeadForm.byPost();
            data.submit = App.LeadForm.submit();

            if (App.LeadForm.error == 0) {
                $.ajax({
                    uri: 'index.php',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        App.AjaxLoader.show();
                    },
                    success: function (r) {

                        if (r.debug) {
                            console.log(r.debug);
                        }

                        $('.lead-response-msg').remove();

                        if (r.response & r.response == 'success') {
                            $('.lead-form').prepend('<div class = "lead-response-msg lead-success">'+ r.message+'</div>');
                            App.AjaxLoader.hide();
                            alert(r.message);
                        } else {
                            var message = 'Unable to submit, please try later.';
                            if (r.message) {
                                message = r.message;
                            }
                            $('.lead-form').prepend('<div class = "lead-response-msg lead-fail">'+ message+'</div>');
                            App.AjaxLoader.hide();
                        }
                    }
                })
            } else {
                var html = '';
                $.each(App.LeadForm.message, function (key, val) {
                    html += '<span class="error-' + key + '">' + val + '<br /></span>';
                });
                $('.error-message').html(html);
            }
        }
    }

    App.LeadForm = {
        error: 0,
        message: {},
        el_config: '#lead-eng-config',
        el_title: '#lead-eng-title',
        el_firstName: '#lead-eng-first-name',
        el_lastName: '#lead-eng-last-name',
        el_email: '#lead-eng-email',
        el_phone: '#lead-eng-phone',

        config: function () {
            return $(this.el_config).val();
        },

        title: function () {
            if ($.trim($(this.el_title).val()) == '') {
                this.error = 1;
                $(this.el_title).addClass('error');
                $(this.el_title).attr('data-error', 'error-title');
                this.message.title = 'Please select title';
                return null;
            }
            return $(this.el_title).val();
        },
        first_name: function () {

            if ($.trim($(this.el_firstName).val()) == '' || $(this.el_firstName).val().length > 55) {
                this.error = 1;
                $(this.el_firstName).addClass('error');
                $(this.el_firstName).attr('data-error', 'error-firstname');
                this.message.firstname = 'Please enter first name';

                return null;
            }
            return $(this.el_firstName).val();
        },
        last_name: function () {
            if ($.trim($(this.el_lastName).val()) == '' || $(this.el_lastName).val().length > 55) {
                this.error = 1;
                $(this.el_lastName).addClass('error');
                $(this.el_lastName).attr('data-error', 'error-lastname');
                this.message.lastname = 'Please enter last name';
                return null;
            }
            return $(this.el_lastName).val();
        },
        phone: function () {
            if ($.trim($(this.el_phone).val()) == '' || $(this.el_phone).val().length < 10 || $(this.el_phone).val().length > 15) {
                this.error = 1;
                $(this.el_phone).addClass('error');
                $(this.el_phone).attr('data-error', 'error-phone');
                this.message.phone = 'Please enter valid phone number.';
                return null;
            }
            return $(this.el_phone).val();
        },
        email: function () {
            if ($.trim($(this.el_email).val()) == '') {
                this.error = 1;
                $(this.el_email).addClass('error');
                $(this.el_email).attr('data-error', 'error-email');
                this.message.email = 'Please enter valid email.';
                return null;
            }
            return $(this.el_email).val();
        },
        byPost: function () {
            if ($('#bypost').attr('checked')) {
                return $('#bypost').val();
            } else {
                return '';
            }
        },
        byEmail: function () {
            if ($('#byemail').attr('checked')) {
                return $('#byemail').val();
            } else {
                this.error = 1;
                $('#byemail').addClass('error');
                $('#byemail').attr('data-error', 'error-byemail');
                this.message.byemail = 'Please accept receive information via email.';
                return null;
            }
        },
        byPhone: function () {
            if ($('#byphone').attr('checked')) {
                return $('#byphone').val();
            } else {
                this.error = 1;
                $('#byphone').addClass('error');
                $('#byphone').attr('data-error', 'error-byphone');
                this.message.byphone = 'Please accept receive information over the phone..';
                return null;
            }
        },
        submit: function () {
            return 'lead_eng_submit'
        },
        reset: function () {
            this.error = 0;
            this.message = {};
            $('.lead-response-msg').remove();
        }

    }

    App.Events = {
        init: function () {
            //Form on submit event.
            $('#lead-eng-submit').click(function (e) {
                e.preventDefault();
                App.LeadEngine.init();
            });

            //On error keypress reset field
            $('.lead-form').on('keyup keypress blur change', '.error', function () {
                var el = $(this).data('error');
                $(this).removeClass('error');
                $('.'+el).remove();
            });
        }
    }

    App.AjaxLoader = {
        template: '<div id="loading"><img src="/wp-content/plugins/lead-eng/assets/ajax-loader.gif" /> </div>',
        show: function () {
            $('.lead-form').prepend(this.template);
            $('#loading').css('height',$('.lead-form').height()+28);
            $('#loading').css('width',$('.lead-form').width()+33);

            $('#loading img').css('margin-left', ($('.lead-form').width() /2) - 30);
            $('#loading img').css('margin-top', ($('.lead-form').height() /2) - 50);
        },
        hide: function () {
            $('#loading').remove();
        }
    }
}(jQuery));

jQuery(function ($) {
    App.Events.init();
});
