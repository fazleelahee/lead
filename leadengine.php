<?php
/*
Plugin Name: Lead engine
Plugin URI: #
Description: Lead engine submission
Author: Tahmina Akter
Version: 1.0
Author URI: #
*/

include_once 'FormData.php';
include_once 'Api.php';
include_once 'functions.php';

// wordpress init hook
add_action('init', 'submit_lead_engine_form_request');

//short code
add_shortcode('LeadForm', 'lead_form');

//load assets
add_action('wp_enqueue_scripts','lead_engine_assets');