<form class="lead-form" id="form-wrapper">
    <span class="error-message"></span>
    <input type="hidden" value="{config}" name="config" id="lead-eng-config"/>

    <div class="top">
        <div class="title">
            Title:
            <br/>
            <select name="title" id="lead-eng-title" class="lead-eng-title">
                <option value="Mr">Mr</option>
                <option value="Mrs">Mrs</option>
            </select>
        </div>

        <div class="firstname">
            <p>
                First Name: </p>

            <p><input name="first_name" type="text" value="" id="lead-eng-first-name" class="lead-eng-first-name"></p>
        </div>

        <div class="lastname">
            <p>Last Name: </p>

            <p><input name="last_name" type="text" value="" id="lead-eng-last-name" class="lead-eng-last-name"></p>

        </div>
    </div>
    <div class="bottom">
        <p>
            Phone
        </p>

        <p>
            <input name="phone" type="text" value="" id="lead-eng-phone">
        </P>

        <p>
            Email
        </p>

        <p>
            <input name="email" type="text" value="" id="lead-eng-email">
        </p>
        <br/>
    </div>
    <div class="optin-text">

        <p>
            I am happy to receive information in the future from Guides &amp; Brochures Limited and Select Property
        </p>

        <div id="optInPost">by post <input id="bypost" type="checkbox" value="1" name="optInPost" checked=""></div>
        <div id="optInEmail">by email <input id="byemail" type="checkbox" value="1" name="optInEmail" checked=""></div>
        <div id="optInTelephone">by telephone <input id="byphone" type="checkbox" value="1" name="optInTelephone" checked=""></div>
    </div>
    <br/>

    <div class="request">
        <input class="requestguide-submit" name="submit" type="submit" value="Request Guide" id="lead-eng-submit">
    </div>

</form>