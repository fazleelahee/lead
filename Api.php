<?php
class Api {
	private $url = 'http://leadengine.guidesandbrochures.co.uk/lead/save-by-params';
	private $response = '';
	private $data_to_submit = array();

	public function setDataToSubmit($data_to_submit)
	{
		$this->data_to_submit = $data_to_submit;
		return $this;
	}

	public function getDataToSubmit()
	{
		return $this->data_to_submit;
	}

	public function getResponse() {
		return $this->response;
	}

	public function connect() {
		$h = curl_init();
		curl_setopt($h, CURLOPT_URL, $this->url);
		curl_setopt($h, CURLOPT_POST, true);
		curl_setopt($h, CURLOPT_POSTFIELDS, $this->getDataToSubmit());
		curl_setopt($h, CURLOPT_HEADER, false);
		curl_setopt($h, CURLOPT_RETURNTRANSFER, 1);
		$this->response = curl_exec($h);
		return $this;
	}
	public function __construct() {}
}