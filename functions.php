<?php

function submit_lead_engine_form_request() {
    if(isset($_POST['submit']) && $_POST['submit'] == 'lead_eng_submit') {
        $rtn  = array();
        $fd = New FormData();

        try{
            $fd->setEmail(@$_POST['email'])
                ->setFirstName(@$_POST['first_name'])
                ->setLastName(@$_POST['last_name'])
                ->setTitle(@$_POST['title'])
                ->setTpg23(@$_POST['phone'])
			    ->setOptInEmail(@$_POST['byEmail'])
			    ->setOptInPost(@$_POST['byPost'])
			    ->setOptInTelephone(@$_POST['byPhone'])
                ->configureFormData(@$_POST['config']);
            $api = New Api();
            $response = $api->setDataToSubmit($fd->toArray())
                ->connect()->getResponse();

            if(preg_match('/lead accepted/', $response)){
                $rtn['message'] = 'Successfully submitted';
                $rtn['response']  = 'success';
            } else {
                $rtn['message'] = $response;
                $rtn['response']  = 'fail';
            }

        } catch (Exception $e){
            $rtn['message'] = $e->getMessage();
            $rtn['response']  = 'fail';
        }

		$rtn['debug'] = $fd->toArray();

        echo json_encode($rtn);
        die();
    }
}

function lead_form( $atts ) {
    $path = dirname(__FILE__);
    $serialized_atts = base64_encode(serialize($atts));
    $form = file_get_contents($path.DIRECTORY_SEPARATOR.'form.php');
    $form = str_replace('{config}',$serialized_atts,  $form);
    return $form;
}

function lead_engine_assets() {
    wp_enqueue_script(
        'leadenginejs',
        plugins_url( '/assets/lead-engine.js' , __FILE__ ),
        array( 'jquery' )
    );

    wp_enqueue_style('leadenginejs', plugins_url( '/assets/lead-engine.css' , __FILE__ ),false,'1.1','all');
}


